5/15 set up mongo db, get and create user, signup

5/16 added in authenticator and accounts instead of user, some file clean up

5/17 worked on crud operations for cocktails.

5/22 added in rough ratings backend, merge resolutions, cocktail detail page

5/23 profile card template, helped debug redux login issues

5/24 merge resolutions, redux conversion for our fetch data

5/25 worked on ratings endpoints in redux and getting info onto the page. more work on detail page.

5/26 worked more on details page adding transition when click rate this cocktail to make a new rating

5/30 added total user ratings and if user has rating will set and display current rating. pytest for get_all_user_ratings

6/1 css for details page

6/2 css for details page and fixing boxes. touch up backend points

6/3 clean up print and console logs. more detail css

6/5 used mike's favorite icon as a base for completing the rating star system

6/6 - 6/9 final touch ups on project, clearing errors, deleting comments, made sure every page was working with any change.
