from pydantic import BaseModel
from typing import List


class FavoriteIn(BaseModel):
    cocktail_name: str


class FavoriteOut(FavoriteIn):
    id: str
    account_id: str


class FavoritesList(BaseModel):
    favorites: List[FavoriteOut]


class DeleteStatus(BaseModel):
    success: bool
