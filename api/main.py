from fastapi import FastAPI
from authenticator import authenticator
from routers import accounts, cocktails, favorites, ratings
from fastapi.middleware.cors import CORSMiddleware
import os

app = FastAPI()

origins = [
    os.environ.get("REACT_APP_API_HOST", ""),
    "https://www.barcompanion.com",
    "https://bar-buddy-89c00064cf94.herokuapp.com",
    "https://bar-buddy-staging-8f2e2b00f8d9.herokuapp.com",
    "https://bar-buddy-api-2d6840079303.herokuapp.com",
    "serverlessinstance0.ahkgaum.mongodb.net",
    os.environ.get("CORS_HOST", None),
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


app.include_router(authenticator.router, tags=["Login"])
app.include_router(accounts.router, tags=["Sign_Up"])
app.include_router(cocktails.router, tags=["Cocktails"])
app.include_router(favorites.router, tags=["Favorites"])
app.include_router(ratings.router, tags=["Ratings"])
