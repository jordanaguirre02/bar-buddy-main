from fastapi.testclient import TestClient
from main import app
from queries.favorites import FavoritesQueries
from authenticator import authenticator

client = TestClient(app)


class FakeFavoriteQueries:
    def delete(self, id: str, account_id: str):
        return True


def fake_get_current_account_data():
    return {"id": "1337"}


def test_delete_favorite():
    # Arrange
    app.dependency_overrides[FavoritesQueries] = FakeFavoriteQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    # Act
    res = client.delete("/api/favorites/123")
    data = res.json()

    # Assert
    assert res.status_code == 200
    assert data == {"success": True}

    # Cleanup
    app.dependency_overrides = {}
