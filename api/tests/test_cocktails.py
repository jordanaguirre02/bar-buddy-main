from fastapi.testclient import TestClient
from main import app
from queries.cocktails import CocktailQueries
from authenticator import authenticator

client = TestClient(app)

# client.get('api/cocktails/{cocktail.id)}')


class FakeCocktailQueries:
    def get_one_cocktail(self, cocktail_id: str):
        return {
            "cocktail_name": "JasmineJuice",
            "alcoholic": True,
            "ingredients": "Magic fairy dust",
            "image_url": "string",
            "instructions": "Drink till you cant drink anymore",
            "suggested_serving_glass": "Tumbler",
            "category": "cocktail",
            "id": cocktail_id,
            "account_id": "646f9c048739e49601a40fce",
        }


def fake_try_get_current_account_data():
    return {"id": "666"}


def test_get_one_cocktail():
    # Arrange
    app.dependency_overrides[CocktailQueries] = FakeCocktailQueries
    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = fake_try_get_current_account_data
    # Act
    res = client.get("/api/cocktails/id")
    data = res.json()
    # Assert
    assert res.status_code == 200
    assert data["id"] == "id"

    # cleanup
    app.dependency_overrides = {}
