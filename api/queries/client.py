from pymongo import MongoClient
from pymongo.errors import ConnectionFailure
from pymongo.server_api import ServerApi
import os


MONGO_USER = os.environ.get("MONGO_USER", "")
MONGO_PASSWORD = os.environ.get("MONGO_PASSWORD", "")
MONGO_HOST = os.environ.get("MONGO_HOST", "")
MONGO_DB = os.environ.get("MONGO_DB", "")

uri = f"mongodb+srv://bbgod7:{MONGO_PASSWORD}@{MONGO_HOST}"
print(uri)
client = MongoClient(uri, tls=True)
try:
    # The ping command is cheap and does not require auth.
    client.admin.command('ping')
except ConnectionFailure:
    print("Server not available")
    print(client)
db = client[MONGO_DB]

class MongoQueries:
    @property
    def collection(self):
        return db[self.collection_name]
