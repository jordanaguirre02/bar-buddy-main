from queries.client import MongoQueries
from bson.objectid import ObjectId
from models.cocktails import (
    CocktailIn,
    CocktailOut,
)


class CocktailQueries(MongoQueries):
    collection_name = "cocktails"

    def get_all_cocktails(self):
        result = []
        cocktail_db = list(self.collection.find({}))
        for cocktail in cocktail_db:
            cocktail["id"] = str(cocktail["_id"])
            del cocktail["_id"]
            result.append(dict(cocktail))
        return result

    def create_cocktail(self, account_id: str, cocktail_in: CocktailIn):
        input_data = cocktail_in.dict()
        input_data["account_id"] = str(account_id)

        result = self.collection.insert_one(input_data)
        if result is None:
            return None

        input_data["id"] = str(result.inserted_id)
        return CocktailOut(**input_data)

    def get_one_cocktail(self, cocktail_id: str):
        result = self.collection.find_one(
            {"_id": ObjectId(cocktail_id)}
            )
        if not result:
            return None
        result["id"] = str(result["_id"])
        del result["_id"]
        return CocktailOut(**result)

    def delete(self, cocktail_id: str, account_id: str) -> bool:
        delete = self.collection.delete_one(
            {"_id": ObjectId(cocktail_id), "account_id": account_id}
        )
        if delete.deleted_count > 0:
            return True
        return False

    def update(
        self, cocktail_id: str, account_id: str, cocktail_in: CocktailIn
    ) -> CocktailOut:
        _ = self.collection.update_one(
            {"_id": ObjectId(cocktail_id)},
            {"$set": {"account_id": account_id, **cocktail_in.dict()}},
        )
        updated_cocktail = self.collection.find_one(
            {"_id": ObjectId(cocktail_id)}
            )
        if updated_cocktail is None:
            return None
        updated_cocktail["id"] = str(updated_cocktail["_id"])
        del updated_cocktail["_id"]

        return CocktailOut(**updated_cocktail)
