from queries.client import MongoQueries
from models.accounts import AccountIn, AccountOutWithPassword, AccountOut
from models.errors import DuplicateAccountError
from bson.objectid import ObjectId


class AccountQueries(MongoQueries):
    collection_name = "accounts"

    def create(self, info: AccountIn, hashed_password: str):
        account = info.dict()
        if self.get_one_by_email(account["email"]):
            raise DuplicateAccountError
        account["hashed_password"] = hashed_password
        del account["password"]
        response = self.collection.insert_one(account)
        if response.inserted_id:
            account["id"] = str(response.inserted_id)
        return AccountOutWithPassword(**account)

    def get_one_by_email(self, email: str):
        result = self.collection.find_one({"email": email})
        if result is None:
            return None
        result["id"] = str(result["_id"])
        return AccountOutWithPassword(**result)

    def get_one_account(self, id: str):
        result = self.collection.find_one({"_id": ObjectId(id)})
        if result is None:
            return None
        result["id"] = str(result["_id"])
        return AccountOut(**result)
