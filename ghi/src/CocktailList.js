import React from 'react';
import { useGetCocktailListQuery } from './app/cocktailSlice';
import CocktailDeleteButton from './CocktailDeleteButton';
import CocktailUpdateButton from './CocktailUpdateButton';

function CocktailList() {
    const { data, isLoading } = useGetCocktailListQuery();


    if (isLoading) {
        return <div>Loading...</div>;
    }

    return (
        <div className="container">
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Picture</th>
                        <th>Name</th>
                        <th>Delete</th>
                        <th>Update</th>

                    </tr>
                </thead>
                <tbody>
                    {data.cocktails.map((cocktail) => (
                        <tr key={cocktail.cocktail_name}>
                            <td><img src={cocktail.image_url} width="300" /></td>
                            <td>{cocktail.cocktail_name}</td>
                            <td> <CocktailDeleteButton props={cocktail.id} /></td>
                            <td> <CocktailUpdateButton props={cocktail.id} /></td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default CocktailList;
















// import React, { useEffect, useState } from 'react';
// import { useGetAccountQuery, useGetCocktailListQuery } from './app/cocktailSlice';


// function CocktailList() {
//     const { data } =useGetCocktailListQuery();


//     return (
//         <>
//             <div className="container">
//                 <table className="table table-striped">
//                     <thead>
//                         <tr>
//                             <th>Name</th>
//                         </tr>
//                     </thead>
//                     <tbody>
//                         {data.map(cocktail => {
//                             return (
//                                 <tr key={cocktail.cocktail_name}>

//                                 </tr>
//                             );
//                         })}
//                     </tbody>
//                 </table>
//             </div>
//         </>
//     )
// }
// export default CocktailList
