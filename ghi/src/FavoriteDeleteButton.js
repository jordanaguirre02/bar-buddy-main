import React from 'react';
import { useDeleteFavoriteMutation } from './app/cocktailSlice';


const FavoriteDeleteButton = (props) => {

    const [getDelete] = useDeleteFavoriteMutation();

    const handleDelete = async () => {
        try {
            getDelete(props.props);
        } catch (error) {
            console.log('Error deleting favorite:', error);
        }
    };

    return (
        <button className="btn-no" onClick={handleDelete}>
            Delete
        </button>
    );
};

export default FavoriteDeleteButton;
