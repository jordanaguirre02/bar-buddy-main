import { useGetAccountQuery, useLogoutMutation } from "./app/cocktailSlice"
import { useNavigate, NavLink } from "react-router-dom"
import './App.css';


function Nav() {

  const { data: account } = useGetAccountQuery();
  const navigate = useNavigate();
  const [logout] = useLogoutMutation();


  return (
    <nav className="navbar navbar-expand-lg navbar-dark">
      <NavLink className="nav-link active" aria-current="page" to="/">
        <img src={"https://media.istockphoto.com/id/180299542/vector/neon-sign-cocktail-lounge-icon.jpg?s=612x612&w=0&k=20&c=M0IJ1YGuGCHMtj0-uloayaNz67p05AEKwlPjRW65NG4="}
          className="sm-img" alt="Home Page" /></NavLink>
          <h1>Bar Companion</h1><br />
      <div className="container-fluid">
        <button className="navbar-toggler" type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
          aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
            </li>
            {!account && <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="login">Login</NavLink>
            </li>}
            {!account && <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="signup">Sign Up</NavLink>
            </li>}
            {account && <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="profile">Profile</NavLink>
            </li>}
            {account && <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="cocktails/new">Create Cocktail</NavLink>
            </li>}
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="cocktails">Cocktails</NavLink>
            </li>
          </ul>
          {account &&
            <button
              className="btn-no btn-sm"
              onClick={() => {
                logout()
                navigate("/")
              }}
            >Logout</button>
          }

        </div>
      </div>
    </nav>
  )
}

export default Nav;
